<?php

namespace Database\Factories;

use App\Models\Role;
use App\Models\Tracker;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\TrackerUser>
 */
class TrackerUserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'tracker_id' => Tracker::get()->random()->id,
            'user_id' => User::get()->random()->id,
            'role_id' => Role::get()->random()->id
        ];
    }
}
