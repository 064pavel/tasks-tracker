<?php

namespace Database\Factories;

use App\Models\Priority;
use App\Models\Status;
use App\Models\Tracker;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Task>
 */
class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $yesterday = Carbon::yesterday();
        $oneWeekAhead = Carbon::now()->addWeek();

        $status = Status::get()->random();
        $user = $status->id == 1 ? null : User::get()->random();

        return [
            'title' => fake()->text(12),
            'short_description' => fake()->text(15),
            'full_description' => fake()->text(150),
            'status_id' => $status->id,
            'priority_id' => Priority::get()->random()->id,
            'user_id' => $user ? $user->id : null,
            'tracker_id' => Tracker::get()->random()->id,
            'views_count' => fake()->numberBetween(0, 999),
            'due_time' => fake()->dateTimeBetween($yesterday, $oneWeekAhead)
        ];
    }
}
