<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Avatar;
use App\Models\Comment;
use App\Models\Task;
use App\Models\Tracker;
use App\Models\TrackerUser;
use App\Models\User;
use Database\Factories\TaskFactory;
use Database\Factories\TrackerUserFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $roles = [
            ['id' => 1, 'role' => 'member'],
            ['id' => 2, 'role' => 'reviewer'],
            ['id' => 3, 'role' => 'owner'],
        ];

        $priorities = [
            ['id' => 1, 'name' => 'Low'],
            ['id' => 2, 'name' => 'Middle'],
            ['id' => 3, 'name' => 'High'],
        ];

        $statuses = [
            ['id' => 1, 'name' => 'untracked'],
            ['id' => 2, 'name' => 'frozen'],
            ['id' => 3, 'name' => 'planned'],
            ['id' => 4, 'name' => 'in progress'],
            ['id' => 5, 'name' => 'in review'],
            ['id' => 6, 'name' => 'completed'],
            ['id' => 7, 'name' => 'overdue'],
        ];


        DB::table('roles')->insert($roles);

        DB::table('priorities')->insert($priorities);

        DB::table('statuses')->insert($statuses);

//        Фабрики для локальной разработки
//        При деплое заккоментировать

//        User::factory(200)->create();
//
//        Tracker::factory(10)->create();
//
//        TrackerUser::factory(50)->create();
//
//        Task::factory(450)->create();
//
//        Comment::factory(300)->create();

    }
}
