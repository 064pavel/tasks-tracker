<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('short_description');
            $table->text('full_description');

            $table->foreignId('status_id')->default(2)->constrained()->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreignId('priority_id')->default(1)->constrained()->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreignId('user_id')->nullable()->constrained()->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreignId('tracker_id')->constrained()->cascadeOnUpdate()->cascadeOnDelete();

            $table->integer('views_count')->default(1);

            $table->dateTime('due_time')->default(today());
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
};
