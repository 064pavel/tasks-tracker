<?php

use App\Http\Controllers\GetPrioritiesController;
use App\Http\Controllers\GetStatusesController;
use App\Http\Controllers\GetUserRoleController;
use App\Http\Controllers\Platform\CreateController;
use App\Http\Controllers\Platform\GetTrackersWhereAllController;
use App\Http\Controllers\Platform\GetTrackersWhereMemberController;
use App\Http\Controllers\Platform\GetTrackersWhereOwnerController;
use App\Http\Controllers\Platform\LeaveTrackerController;
use App\Http\Controllers\Platform\Owner\DeleteTrackerController;
use App\Http\Controllers\Platform\Settings\ProfileUpdateController;
use App\Http\Controllers\Platform\StoreController;
use App\Http\Controllers\PlatformController;
use App\Http\Controllers\Statistic\Profile\GetProfileInfo;
use App\Http\Controllers\Statistic\Tracker\TaskPriorityRatioController;
use App\Http\Controllers\Statistic\Tracker\TaskRatioController;
use App\Http\Controllers\Statistic\Tracker\TaskStatusRatioController;
use App\Http\Controllers\Statistic\Tracker\TopIssueFixerController;
use App\Http\Controllers\Statistic\Tracker\TrackerInfoController;
use App\Http\Controllers\TaskInfo\DeleteTaskController;
use App\Http\Controllers\TaskInfo\EditTaskController;
use App\Http\Controllers\TaskInfo\GetTaskInfoController;
use App\Http\Controllers\TaskInfo\StoreCommentController;
use App\Http\Controllers\TaskInfo\UpdateTaskController;
use App\Http\Controllers\Tracker\Archive\TaskArchiveController;
use App\Http\Controllers\Tracker\Archive\TaskRestoreController;
use App\Http\Controllers\Tracker\Settings\GetTrackerController;
use App\Http\Controllers\Tracker\Settings\UpdateTrackerController;
use App\Http\Controllers\Tracker\Team\AddMemberController;
use App\Http\Controllers\Tracker\Team\DeleteMemberController;
use App\Http\Controllers\Tracker\Team\GetMembersController;
use App\Http\Controllers\Tracker\Workflow\CreateTaskController;
use App\Http\Controllers\Tracker\Workflow\GetTasksAvailableController;
use App\Http\Controllers\Tracker\Workflow\GetTasksInProgressController;
use App\Http\Controllers\Tracker\Workflow\GetUsersByTrackerController;
use App\Http\Controllers\Tracker\Workflow\StoreTaskController;
use App\Http\Controllers\Tracker\Workflow\TakeATaskController;
use App\Http\Controllers\Tracker\Workspace\ChangeTaskStatusController;
use App\Http\Controllers\Tracker\Workspace\GetCompletedTasksController;
use App\Http\Controllers\Tracker\Workspace\GetTasksByTrackerForWorkspaceController;
use App\Http\Controllers\TrackerController;
use App\Http\Controllers\User\GetUserController;
use App\Http\Controllers\User\UploadAvatarController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware('auth')->group(function () {

    Route::prefix('statistic')->group(function () {

        Route::prefix('trackers/{id}')->group(function () {
            Route::get('/task-status-ratio', TaskStatusRatioController::class)->name('statistic.tracker.task-status-ratio');
            Route::get('/task-priority-ratio', TaskPriorityRatioController::class)->name('statistic.tracker.task-priority-ratio');
            Route::get('/info', TrackerInfoController::class)->name('statistic.tracker.info');
            Route::get('/task-ratio', TaskRatioController::class)->name('statistic.tracker.task-ratio');
            Route::get('/top-issue-fixer', TopIssueFixerController::class);
        });

        Route::prefix('profile')->group(function () {
            Route::get('/info', GetProfileInfo::class)->name('statistic.profile.info');
        });
    });

    Route::get('/get-user', GetUserController::class)->name('get-user');

    Route::prefix('platform')->group(function () {

        Route::prefix('trackers')->group(function () {
            Route::post('/store', StoreController::class)->name('platform.trackers.store');
            Route::get('/create', CreateController::class)->name('platform.trackers.create');
            Route::get('/all', GetTrackersWhereAllController::class)->name('platform.trackers.all');
            Route::get('/owner', GetTrackersWhereOwnerController::class)->name('platform.trackers.owner');
            Route::get('/member', GetTrackersWhereMemberController::class)->name('platform.trackers.member');
            Route::delete('/{tracker}', DeleteTrackerController::class)->name('platform.tracker.delete');
            Route::delete('/{id}/leave', LeaveTrackerController::class)->name('platform.tracker.leave');
        });

        Route::prefix('profile')->group(function () {
            Route::post('/upload-avatar', UploadAvatarController::class)->name('platform.profile.upload-avatar');
        });


        Route::get('/', PlatformController::class)->name('platform');
    });

    Route::prefix('trackers')->group(function () {

        Route::get('/get-info/{tracker}', GetTrackerController::class)->name('tracker.get-info');

        Route::prefix('{tracker}')->middleware('tracker.access')->group(function () {

            Route::get('/', TrackerController::class)->name('tracker');
            Route::get('/get-role', GetUserRoleController::class)->name('tracker.get-role');
            Route::put('/update', UpdateTrackerController::class)->name('tracker.update');

            Route::prefix('tasks')->group(function () {
                Route::get('/archive', TaskArchiveController::class)->name('tracker.tasks.archive');
                Route::patch('/{task}/restore', TaskRestoreController::class)->name('tracker.tasks.archive.restore');
                Route::get('/create', CreateTaskController::class)->middleware('tracker.owner.access')->name('tracker.workflow.tasks.create');
                Route::post('/store', StoreTaskController::class)->name('tracker.workflow.tasks.store');
                Route::get('/{task}', GetTaskInfoController::class)->name('tracker.tasks.info');
                Route::get('/{task}/edit', EditTaskController::class)->middleware('tracker.owner.access')->name('tracker.tasks.info.edit');
                Route::post('/{task}/comment', StoreCommentController::class)->name('tracker.tasks.comment.store');
                Route::delete('/{task}', DeleteTaskController::class)->name('tracker.tasks.delete');

            });


            Route::get('/tasks', GetTasksByTrackerForWorkspaceController::class)->name('tracker.workspace.tasks');
            Route::get('/tasks/get/completed', GetCompletedTasksController::class)->name('tracker.workspace.tasks.completed');

            Route::get('/get-users-by-tracker', GetUsersByTrackerController::class)->name('tracker.get-users-by-tracker');
            Route::get('/get-tasks-in-progress', GetTasksInProgressController::class)->name('tracker.workspace.get-tasks-in-progress');
            Route::get('/get-tasks-available', GetTasksAvailableController::class)->name('trackers.workspace.get-tasks-available');

            Route::post('/users/{user}', AddMemberController::class)->name('trackers.team.members.add');
            Route::delete('/users/{user}', DeleteMemberController::class)->name('tracker.team.members.delete');
            Route::get('/team/members', GetMembersController::class)->name('tracker.team.members');
        });

        Route::put('/tasks/{id}/update', UpdateTaskController::class)->name('tracker.tasks.update');
        Route::patch('/tasks/{task}/take', TakeATaskController::class)->name('tracker.workflow.tasks.take');


        Route::patch('/tasks/{id}/change-status/increment', [ChangeTaskStatusController::class, 'increment'])->name('tracker.workspace.tasks.change-status-increment');
        Route::patch('/tasks/{id}/change-status/decrement', [ChangeTaskStatusController::class, 'decrement'])->name('tracker.workspace.tasks.change-status-decrement');

    });

    Route::get('/statuses', GetStatusesController::class)->name('statuses');
    Route::get('/priorities', GetPrioritiesController::class)->name('priorities');

});


require __DIR__ . '/auth.php';
