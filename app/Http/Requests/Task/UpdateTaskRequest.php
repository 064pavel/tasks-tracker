<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string|min:1|max:12',
            'short_description' => 'required|string|min:3|max:30',
            'full_description' => 'required|string|min:10|max:200',
            'status_id' => 'required|integer',
            'priority_id' => 'required|integer',
            'user_id' => 'nullable|integer',
            'tracker_id' => 'required|integer',
            'due_time' => 'required|date',
            'views_count' => 'nullable|integer',
        ];
    }
}
