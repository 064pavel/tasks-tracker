<?php

namespace App\Http\Middleware;

use App\Models\TrackerUser;
use Closure;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Symfony\Component\HttpFoundation\Response;

class TrackerOwnerAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response) $next
     */
    public function handle($request, Closure $next): \Inertia\Response|Response
    {

        $trackerId = $request->route('tracker');
        $userId = Auth::id();

        $trackerUser = TrackerUser::where('tracker_id', $trackerId)
            ->where('user_id', $userId)
            ->where('role_id', 3)
            ->first();

        if ($trackerUser) {
            return $next($request);
        } else {
            return Inertia::render('NoAccessPage');
        }

    }
}
