<?php

namespace App\Http\Middleware;

use App\Models\TrackerUser;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Symfony\Component\HttpFoundation\Response;

class TrackerAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle($request, Closure $next): \Inertia\Response | Response
    {
        $trackerId = $request->route('tracker');

        $trackerUser = TrackerUser::where('tracker_id', $trackerId)
            ->where('user_id', Auth::id())
            ->first();

        if ($trackerUser){
            return $next($request);
        } else {
            return Inertia::render('NoAccessPage');
        }
    }
}
