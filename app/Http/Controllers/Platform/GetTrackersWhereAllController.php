<?php

namespace App\Http\Controllers\Platform;

use App\Http\Controllers\Controller;
use App\Http\Resources\Platform\TrackerUserResource;
use App\Models\TrackerUser;
use Illuminate\Support\Facades\Auth;


class GetTrackersWhereAllController extends Controller
{
    public function __invoke(): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        return TrackerUserResource::collection(
            TrackerUser::where('user_id', Auth::id())
                ->orderBy('created_at', 'desc')
                ->get());
    }
}
