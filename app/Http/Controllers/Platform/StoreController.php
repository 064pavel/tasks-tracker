<?php

namespace App\Http\Controllers\Platform;

use App\Http\Controllers\Controller;
use App\Http\Requests\Tracker\StoreTrackerRequest;
use App\Models\Tracker;
use App\Models\TrackerUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;
use Psy\Util\Json;

class StoreController extends Controller
{
    public function __invoke(StoreTrackerRequest $request): \Illuminate\Http\RedirectResponse | \Inertia\Response
    {
        $data = $request->validated();
        $data['user_id'] = Auth::id();

        try {
            DB::beginTransaction();

            $tracker = Tracker::create($data);

            $user = Auth::user();

            TrackerUser::create([
                'tracker_id' => $tracker->id,
                'user_id' => $user->id,
                'role_id' => 3
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->with('error', 'An error occurred while configuring the tracker');
        }

        return redirect()->route('platform');
    }
}
