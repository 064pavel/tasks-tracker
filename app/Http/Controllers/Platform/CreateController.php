<?php

namespace App\Http\Controllers\Platform;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CreateController extends Controller
{
    public function __invoke(): \Inertia\Response
    {
        return Inertia::render('Platform/Panel/CreateTracker/CreateTracker');
    }
}
