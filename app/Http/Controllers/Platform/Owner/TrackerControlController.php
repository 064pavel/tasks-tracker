<?php

namespace App\Http\Controllers\Platform\Owner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

class TrackerControlController extends Controller
{
    public function getTrackerControl(): \Inertia\Response
    {
        return Inertia::render('Tracker/TrackerControl/TrackerControl');
    }
}
