<?php

namespace App\Http\Controllers\Platform\Owner;

use App\Http\Controllers\Controller;
use App\Models\Tracker;
use Illuminate\Http\Request;

class DeleteTrackerController extends Controller
{
    public function __invoke($tracker): \Illuminate\Http\JsonResponse
    {
        try {
            $tracker = Tracker::find($tracker);
            $tracker->delete();
            return response()->json(['success'=>'Tracker removed']);
        }catch (\Exception $e){
            return response()->json(['error' => 'server error'], 500);
        }
    }
}
