<?php

namespace App\Http\Controllers\Platform;

use App\Http\Controllers\Controller;
use App\Http\Resources\Platform\TrackerUserResource;
use App\Models\Tracker;
use App\Models\TrackerUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GetTrackersWhereOwnerController extends Controller
{
    public function __invoke(): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        return TrackerUserResource::collection(
            TrackerUser::where('user_id', Auth::id())
                ->where('role_id', 3)
                ->orderBy('created_at', 'desc')
                ->get());
    }
}
