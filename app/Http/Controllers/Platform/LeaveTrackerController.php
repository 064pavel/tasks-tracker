<?php

namespace App\Http\Controllers\Platform;

use App\Http\Controllers\Controller;
use App\Models\TrackerUser;

class LeaveTrackerController extends Controller
{
    public function __invoke($id): \Illuminate\Http\JsonResponse
    {
        try {
            $trackerUser = TrackerUser::find($id);
            $trackerUser->delete();
            return response()->json(['success' => 'You left the tracker']);
        } catch (\Exception $e){
            return response()->json(['error' => 'server error'], 500);
        }
    }
}
