<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class TrackerController extends Controller
{
    public function __invoke(Request $request): \Inertia\Response
    {
        $tracker_id = $request->tracker;
        return Inertia::render('Tracker/Tracker', compact('tracker_id'));
    }
}
