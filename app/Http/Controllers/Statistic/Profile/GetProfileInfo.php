<?php

namespace App\Http\Controllers\Statistic\Profile;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Task;
use App\Models\TrackerUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GetProfileInfo extends Controller
{
    public function __invoke(): \Illuminate\Http\JsonResponse
    {
        try {
            DB::beginTransaction();

            $taskCount = Task::where('user_id', Auth::id())->count();
            $trackerCount = TrackerUser::where('user_id', Auth::id())->count();
            $commentCount = Comment::where('user_id', Auth::id())->count();

            $result = [
                'task_count' => $taskCount,
                'tracker_count' => $trackerCount,
                'comment_count' => $commentCount,
            ];

            DB::commit();

            return response()->json($result);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => 'Server error'], 500);
        }
    }
}
