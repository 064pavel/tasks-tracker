<?php

namespace App\Http\Controllers\Statistic\Tracker;

use App\Http\Controllers\Controller;
use App\Http\Resources\Platform\UserResource;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TopIssueFixerController extends Controller
{
    public function __invoke($id)
    {
        try {
            $topUsers = Task::where('tracker_id', $id)
                ->where('status_id', 6)
                ->select('user_id', DB::raw('COUNT(*) as count'))
                ->groupBy('user_id')
                ->orderByDesc('count')
                ->limit(3)
                ->get('user_id');

            $userFull = [];

            foreach ($topUsers as $user) {
                $userFull[] = User::where('id', $user['user_id'])->first();
            }

            return UserResource::collection($userFull);
        } catch (\Exception $e) {
            return response()->json(['error' => 'server error'], 500);
        }
    }
}
