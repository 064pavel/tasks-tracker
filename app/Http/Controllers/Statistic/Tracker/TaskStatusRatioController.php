<?php

namespace App\Http\Controllers\Statistic\Tracker;

use App\Http\Controllers\Controller;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TaskStatusRatioController extends Controller
{
    public function __invoke($id): \Illuminate\Http\JsonResponse
    {
        try {
            $tasks = Task::select('statuses.name', DB::raw('COUNT(*) as count'))
                ->join('statuses', 'statuses.id', '=', 'tasks.status_id')
                ->where('tasks.tracker_id', $id)
                ->where('tasks.status_id', '<>', 7)
                ->groupBy('statuses.name')
                ->get();

            $totalTasks = $tasks->sum('count');

            $result = [];

            foreach ($tasks as $task) {
                $percentage = number_format(($task->count / $totalTasks) * 100, 2);
                $result[] = [
                    'status' => $task->name,
                    'percent' => $percentage,
                ];
            }

            return response()->json($result);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
