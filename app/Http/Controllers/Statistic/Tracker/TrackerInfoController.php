<?php

namespace App\Http\Controllers\Statistic\Tracker;

use App\Http\Controllers\Controller;
use App\Models\Task;
use App\Models\Tracker;
use App\Models\TrackerUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TrackerInfoController extends Controller
{
    public function __invoke($id)
    {
        $tracker_description = Tracker::where('id', $id)->first('description');

        $created_at = Tracker::where('id', $id)->value('created_at');
        $created = Carbon::parse($created_at)->format('F d, Y');

        $taskCountInTracker = Task::where('tracker_id', $id)->count();

        $members = TrackerUser::where('tracker_id', $id)->count();

        $commentCount = DB::table('comments')
            ->join('tasks', 'comments.task_id', '=', 'tasks.id')
            ->where('tasks.tracker_id', $id)
            ->count();



        $taskCounts = Task::select('tracker_id', DB::raw('COUNT(*) as count'))
            ->groupBy('tracker_id')
            ->pluck('count', 'tracker_id');

        $userCounts = TrackerUser::select('tracker_id', DB::raw('COUNT(*) as count'))
            ->groupBy('tracker_id')
            ->pluck('count', 'tracker_id');

        $ratios = [];
        foreach ($taskCounts as $trackerId => $taskCount) {
            $userCount = $userCounts->get($trackerId, 0);
            if ($userCount > 0) {
                $ratio = $taskCount / $userCount;
            } else {
                $ratio = 0;
            }
            $ratios[$trackerId] = $ratio;
        }

        arsort($ratios);

        $topTrackers = array_keys($ratios);

        $top = array_search($id, $topTrackers) + 1;

        return response()->json([
            'description' => $tracker_description,
            'created' => $created,
            'members' => $members,
            'comment_count' => $commentCount,
            'taskCount' => $taskCountInTracker,
            'top' => $top
        ]);
    }
}
