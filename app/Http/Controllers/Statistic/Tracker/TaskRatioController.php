<?php

namespace App\Http\Controllers\Statistic\Tracker;

use App\Http\Controllers\Controller;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskRatioController extends Controller
{
    public function __invoke($id): \Illuminate\Http\JsonResponse
    {
        try {
            $tasks = Task::where('tracker_id', $id)->get();

            $totalTasks = count($tasks);
            $onTimeTasks = [];
            $completedTasks = [];
            $overdueTasks = [];

            foreach ($tasks as $task) {
                if ($task->status_id >= 1 && $task->status_id <= 5) {
                    $onTimeTasks[] = $task;
                } elseif ($task->status_id == 6) {
                    $completedTasks[] = $task;
                } elseif ($task->status_id == 7) {
                    $overdueTasks[] = $task;
                }
            }

            $response = [
                'total_tasks' => $totalTasks,
                'on_time_tasks' => [
                    'count' => count($onTimeTasks),
                    'percentage' => round((count($onTimeTasks) / $totalTasks) * 100, 2)
                ],
                'completed_tasks' => [
                    'count' => count($completedTasks),
                    'percentage' => round((count($completedTasks) / $totalTasks) * 100, 2)
                ],
                'overdue_tasks' => [
                    'count' => count($overdueTasks),
                    'percentage' => round((count($overdueTasks) / $totalTasks) * 100, 2)
                ]
            ];

            return response()->json($response);
        } catch (\Exception $e) {
            return response()->json(['error' => 'server error'], 500);
        }
    }

}
