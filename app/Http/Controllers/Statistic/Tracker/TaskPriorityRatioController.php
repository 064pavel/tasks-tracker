<?php

namespace App\Http\Controllers\Statistic\Tracker;

use App\Http\Controllers\Controller;
use App\Models\Priority;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TaskPriorityRatioController extends Controller
{
    public function __invoke($id): \Illuminate\Http\JsonResponse
    {
        DB::beginTransaction();

        try {
            $tasks = Task::select('priority_id', DB::raw('COUNT(*) as count'))
                ->where('tracker_id', $id)
                ->groupBy('priority_id')
                ->get();

            $totalTasks = $tasks->sum('count');

            $result = [];

            foreach ($tasks as $task) {
                $percentage = round(($task->count / $totalTasks) * 100, 2);
                $priority = Priority::find($task->priority_id);

                if (!$priority) {
                    DB::rollback();
                    return response()->json(['error' => 'Priority not found'], 404);
                }

                $priorityName = $priority->name;

                $result[] = [
                    'priority_name' => $priorityName,
                    'count' => $task->count,
                    'percentage' => sprintf("%.2f%%", $percentage)
                ];
            }

            DB::commit();

            return response()->json($result);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
