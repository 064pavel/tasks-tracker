<?php

namespace App\Http\Controllers\TaskInfo;

use App\Http\Controllers\Controller;
use App\Http\Resources\Tracker\TaskResource;
use App\Models\Task;
use Illuminate\Http\Request;
use Inertia\Inertia;

class EditTaskController extends Controller
{
    public function __invoke($tracker_id, $task_id)
    {
        $task = Task::find($task_id);
        $task = new TaskResource($task);

        return Inertia::render('Tracker/EditTask', compact('task'));
    }
}
