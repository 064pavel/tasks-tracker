<?php

namespace App\Http\Controllers\TaskInfo;

use App\Http\Controllers\Controller;
use App\Http\Requests\Task\StoreCommentRequest;
use App\Models\Comment;
use Illuminate\Support\Facades\Auth;


class StoreCommentController extends Controller
{
    public function __invoke(StoreCommentRequest $request): \Illuminate\Http\JsonResponse
    {
        try {
            $data = $request->validated();
            $data['user_id'] = Auth::id();
            Comment::create($data);
            return response()->json(['successfully' => 'comment created successfully']);
        } catch (\Exception $e) {
            return response()->json(['error' => 'server error']);
        }
    }
}
