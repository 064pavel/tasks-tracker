<?php

namespace App\Http\Controllers\TaskInfo;

use App\Http\Controllers\Controller;
use App\Http\Resources\Tracker\TaskResource;
use App\Models\Task;
use App\Models\Tracker;
use Illuminate\Database\QueryException;
use Inertia\Inertia;

class GetTaskInfoController extends Controller
{
    public function __invoke($tracker, $task): \Illuminate\Http\JsonResponse | \Inertia\Response
    {
        try {
            $task = Task::findOrFail($task);
            $task->increment('views_count');
            $task = new TaskResource($task);
            return Inertia::render('Tracker/TaskInfo', compact('task'));
        } catch (QueryException $e) {
            return response()->json(['error' => 'SQL query execution error'], 500);
        }
    }
}
