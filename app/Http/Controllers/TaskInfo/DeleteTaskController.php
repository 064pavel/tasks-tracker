<?php

namespace App\Http\Controllers\TaskInfo;

use App\Http\Controllers\Controller;
use App\Models\Task;
use Psy\Util\Json;


class DeleteTaskController extends Controller
{
    public function __invoke($tracker, $task): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
    {
        try {
            $task = Task::find($task);
            $task->delete();

            return response()->json(['message' => 'task deleted']);

        } catch (\Exception $e) {
            return response()->json(['message' => 'An error occurred while deleting the task'], 500);
        }
    }
}


