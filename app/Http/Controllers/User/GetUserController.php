<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GetUserController extends Controller
{
    public function __invoke(): UserResource
    {
        return new UserResource(User::where('id', Auth::id())->first());
    }
}
