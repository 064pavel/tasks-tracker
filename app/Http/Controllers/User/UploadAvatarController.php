<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\AvatarRequest;
use App\Models\Avatar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UploadAvatarController extends Controller
{
    public function __invoke(AvatarRequest $request): \Illuminate\Http\JsonResponse
    {
        try {
            $user = Auth::user();

            $avatar = Avatar::find($user->avatar_id);

            $avatarPath = $request->avatar->store('avatar', 'public');

            if ($avatar) {
                $avatar->avatar_path = $avatarPath;
                $avatar->save();
            } else {
                $avatar = new Avatar();
                $avatar->avatar_path = $avatarPath;
                $avatar->save();
            }

            $user->avatar_id = $avatar->id;
            $user->save();

            return response()->json(['message' => 'Avatar successfully saved']);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Error while saving avatar'], 500);
        }
    }
}
