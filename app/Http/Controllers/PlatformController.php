<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class PlatformController extends Controller
{
    public function __invoke(): \Inertia\Response
    {
        return Inertia::render('Platform/Platform');
    }
}
