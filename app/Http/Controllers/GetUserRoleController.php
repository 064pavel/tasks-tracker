<?php

namespace App\Http\Controllers;

use App\Models\TrackerUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GetUserRoleController extends Controller
{
    public function __invoke($tracker): \Illuminate\Http\JsonResponse
    {
        try {
            $role_id = TrackerUser::where('tracker_id', $tracker)
                ->where('user_id', Auth::id())
                ->pluck('role_id');

            return response()->json(['role_id' => $role_id]);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Server error']);
        }
    }
}
