<?php

namespace App\Http\Controllers\Tracker\Team;

use App\Http\Controllers\Controller;
use App\Http\Resources\Platform\TrackerUserResource;
use App\Models\TrackerUser;
use Illuminate\Http\Request;

class GetMembersController extends Controller
{
    public function __invoke($tracker_id): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        return TrackerUserResource::collection(
            TrackerUser::where('tracker_id', $tracker_id)
                ->get()
        );
    }
}
