<?php

namespace App\Http\Controllers\Tracker\Team;

use App\Http\Controllers\Controller;
use App\Models\TrackerUser;
use Illuminate\Http\Request;

class DeleteMemberController extends Controller
{
    public function __invoke($tracker, $user): \Illuminate\Http\JsonResponse
    {
        try {
            $trackerUser = TrackerUser::where('tracker_id', $tracker)->where('user_id', $user)->first();

            if ($trackerUser) {
                $trackerUser->delete();
                return response()->json(['success' => 'Tracker user record deleted']);
            } else {
                return response()->json(['error' => 'Tracker user record not found'], 404);
            }
        } catch (\Exception $e){
            return response()->json(['error' => 'server error'], 500);
        }
    }
}
