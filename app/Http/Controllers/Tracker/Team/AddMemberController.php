<?php

namespace App\Http\Controllers\Tracker\Team;

use App\Http\Controllers\Controller;
use App\Models\TrackerUser;
use App\Models\User;
use Illuminate\Http\Request;

class AddMemberController extends Controller
{
    public function __invoke($tracker, $user): \Illuminate\Http\JsonResponse
    {
        try {
            $find_user = User::find($user);

            if ($find_user) {

                $existing_member = TrackerUser::where('tracker_id', $tracker)
                    ->where('user_id', $user)
                    ->first();

                if ($existing_member) {
                    return response()->json(['error' => 'User already exists in tracker']);
                } else {
                    TrackerUser::create([
                        'tracker_id' => $tracker,
                        'user_id' => $user,
                        'role_id' => 1
                    ]);

                    return response()->json(['success' => 'User added to tracker']);
                }

            } else {
                return response()->json(['error' => 'There is no user with this ID']);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => 'server error']);
        }
    }
}
