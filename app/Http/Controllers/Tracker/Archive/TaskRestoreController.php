<?php

namespace App\Http\Controllers\Tracker\Archive;

use App\Http\Controllers\Controller;
use App\Models\Task;

class TaskRestoreController extends Controller
{
    public function __invoke($tracker_id, $task_id): \Illuminate\Http\JsonResponse
    {
        try {

            $deletedTask = Task::onlyTrashed()->find($task_id);

            if ($deletedTask) {
                $deletedTask->restore();
            }

            return response()->json(['message' => 'Task restored']);

        } catch (\Exception $e) {
            return response()->json(['error' => 'An error occurred.'], 500);
        }
    }
}
