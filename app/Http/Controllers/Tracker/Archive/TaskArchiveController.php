<?php

namespace App\Http\Controllers\Tracker\Archive;

use App\Http\Controllers\Controller;
use App\Http\Resources\Archive\TaskResource;
use App\Models\Task;


class TaskArchiveController extends Controller
{
    public function __invoke($tracker_id): \Illuminate\Http\JsonResponse | \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        try {
            $deletedTasks = Task::withTrashed()
                ->where('tracker_id', $tracker_id)
                ->whereNotNull('deleted_at')
                ->get();

            return TaskResource::collection($deletedTasks);
        } catch (\Exception $e) {
            return response()->json(['error' => 'An error occurred.'], 500);
        }
    }
}
