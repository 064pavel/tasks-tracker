<?php

namespace App\Http\Controllers\Tracker\Workflow;

use App\Http\Controllers\Controller;
use App\Http\Resources\Tracker\TrackerUserResource;
use App\Http\Resources\UserResource;
use App\Models\TrackerUser;
use App\Models\User;
use Illuminate\Http\Request;

class GetUsersByTrackerController extends Controller
{
    public function __invoke($tracker_id): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        return TrackerUserResource::collection(
            TrackerUser::where('tracker_id', $tracker_id)
                ->get()
        );
    }
}
