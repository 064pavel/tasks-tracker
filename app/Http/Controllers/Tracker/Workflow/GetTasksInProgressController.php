<?php

namespace App\Http\Controllers\Tracker\Workflow;

use App\Http\Controllers\Controller;
use App\Http\Resources\Tracker\TaskResource;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GetTasksInProgressController extends Controller
{
    public function __invoke($tracker_id): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        $tasks = Task::where('tracker_id', $tracker_id)
            ->whereDate('due_time', '>=', Carbon::today())
            ->whereNotIn('status_id', [1,7])
            ->get();

        return TaskResource::collection($tasks);
    }
}
