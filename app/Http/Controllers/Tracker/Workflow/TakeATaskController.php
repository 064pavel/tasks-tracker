<?php

namespace App\Http\Controllers\Tracker\Workflow;

use App\Http\Controllers\Controller;
use App\Models\Task;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TakeATaskController extends Controller
{
    public function __invoke(Task $task): \Illuminate\Http\JsonResponse
    {
        DB::beginTransaction();

        try {
            $countTaskByUser = Task::where('user_id', Auth::id())
                ->where('tracker_id', $task->tracker->id)
                ->count();

            if ($countTaskByUser <= 10) {
                $task->status_id = 2;
                $task->user_id = Auth::id();
                $task->save();

                DB::commit();

                return response()->json(['message' => 'The task is assigned to you and is located in the "Workspace" section']);
            } else {
                return response()->json(['message' => 'You have more than 10 tasks, you cannot take more']);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'An error occurred while updating the task'], 500);
        }
    }
}
