<?php

namespace App\Http\Controllers\Tracker\Workflow;

use App\Http\Controllers\Controller;
use App\Http\Resources\Tracker\TaskResource;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GetTasksAvailableController extends Controller
{
    public function __invoke($tracker_id): \Illuminate\Http\JsonResponse | \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        try {
            $tasks = Task::where('tracker_id', $tracker_id)
                ->whereDate('due_time', '>=', Carbon::today())
                ->where('user_id', null)
                ->where('status_id', 1)
                ->get();

            return TaskResource::collection($tasks);
        } catch (\Exception $e) {
            return response()->json(['error' => 'server error'], 500);
        }
    }
}
