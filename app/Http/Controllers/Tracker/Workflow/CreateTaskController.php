<?php

namespace App\Http\Controllers\Tracker\Workflow;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CreateTaskController extends Controller
{
    public function __invoke($tracker_id): \Inertia\Response
    {
        return Inertia::render('Tracker/WorkflowComponents/CreateTask', compact('tracker_id'));
    }
}
