<?php

namespace App\Http\Controllers\Tracker\Workflow;

use App\Http\Controllers\Controller;
use App\Http\Requests\Task\StoreTaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class StoreTaskController extends Controller
{
    public function __invoke(StoreTaskRequest $request): \Illuminate\Http\RedirectResponse
    {
        $tracker_id = $request->tracker_id;

        $task = $request->validated();

        Task::create($task);

        return redirect()->route('tracker', ['tracker' => $tracker_id]);
    }
}
