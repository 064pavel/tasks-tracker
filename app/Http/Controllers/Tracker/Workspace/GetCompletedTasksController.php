<?php

namespace App\Http\Controllers\Tracker\Workspace;

use App\Http\Controllers\Controller;
use App\Http\Resources\Tracker\TaskResource;
use App\Models\Task;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class GetCompletedTasksController extends Controller
{
    public function __invoke($tracker):  \Illuminate\Http\JsonResponse | \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        try {
            return TaskResource::collection(
                Task::where('tracker_id', $tracker)
                    ->where('user_id', Auth::id())
                    ->where('status_id', 6)
                    ->get()
            );
        } catch (\Exception $e) {
            return response()->json(['error' => 'server error']);
        }
    }
}
