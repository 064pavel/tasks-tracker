<?php

namespace App\Http\Controllers\Tracker\Workspace;

use App\Http\Controllers\Controller;
use App\Models\Task;

class ChangeTaskStatusController extends Controller
{
    public function increment($id): \Illuminate\Http\JsonResponse|int
    {
        try {
            $task = Task::find($id);


            if ($task) {
                $task->increment('status_id');
                return response()->json('status updated');
            } else {
                return response()->json('Task not found', 404);
            }
        } catch (\Exception $e) {
            return response()->json('Error while updating status', 500);
        }
    }

    public function decrement($id): \Illuminate\Http\JsonResponse
    {
        $task = Task::find($id);

        if ($task) {
            $task->decrement('status_id');

            if ($task->status_id === 1) {
                $task->user_id = null;
                $task->save();
            }
        }

        return response()->json('status updated');
    }
}
