<?php

namespace App\Http\Controllers\Tracker\Workspace;

use App\Http\Controllers\Controller;

use App\Http\Resources\Tracker\TaskResource;

use App\Models\Task;
use App\Models\TrackerUser;

use Illuminate\Support\Facades\Auth;

class GetTasksByTrackerForWorkspaceController extends Controller
{
    public function __invoke($tracker_id): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        return TaskResource::collection(
            Task::where('tracker_id', $tracker_id)
                ->where('user_id', Auth::id())
                ->whereNot('status_id', 6)
                ->get()
        );
    }
}
