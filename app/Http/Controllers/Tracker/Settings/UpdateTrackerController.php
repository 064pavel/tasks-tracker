<?php

namespace App\Http\Controllers\Tracker\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\Tracker\StoreTrackerRequest;
use App\Models\Tracker;
use Illuminate\Http\Client\Response;
use Illuminate\Http\Request;

class UpdateTrackerController extends Controller
{
    public function __invoke(StoreTrackerRequest $request, $id): \Illuminate\Http\JsonResponse
    {
        try {
            $tracker = Tracker::findOrFail($id);
            $tracker->update($request->validated());
            return response()->json(['success' => 'Tracker successfully updated']);
        }catch (\Exception $e){
            return response()->json(['error' => 'server error']);
        }
    }
}
