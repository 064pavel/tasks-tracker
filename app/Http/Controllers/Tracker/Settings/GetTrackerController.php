<?php

namespace App\Http\Controllers\Tracker\Settings;

use App\Http\Controllers\Controller;
use App\Http\Resources\Platform\TrackerResource;
use App\Models\Tracker;

class GetTrackerController extends Controller
{
    public function __invoke($tracker): \Illuminate\Http\JsonResponse | TrackerResource
    {
        try {

            $tracker = Tracker::where('id', $tracker)->first();

            return new TrackerResource($tracker);
        } catch (\Exception $e){
            return response()->json(['error' => 'server error']);
        }
    }
}
