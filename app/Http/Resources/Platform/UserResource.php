<?php

namespace App\Http\Resources\Platform;

use App\Http\Resources\AvatarResource;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $taskCount = Task::where('user_id', $this->id)
            ->where('status_id', 6)
            ->count();

        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'created' => $this->created_at->format('F d, Y'),
            'avatar' => new AvatarResource($this->avatar),
            'avatar_id' => $this->avatar_id,
            'task_count' => $taskCount,
        ];
    }
}
