<?php

namespace App\Http\Resources\Platform;

use App\Http\Resources\RoleResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TrackerUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'tracker' => new TrackerResource($this->tracker),
            'user' => new UserResource($this->user),
            'role' => new RoleResource($this->role)
        ];
    }
}
