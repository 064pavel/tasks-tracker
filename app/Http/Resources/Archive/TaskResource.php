<?php

namespace App\Http\Resources\Archive;

use App\Http\Resources\CommentResource;
use App\Http\Resources\Platform\TrackerResource;
use App\Http\Resources\PriorityResource;
use App\Http\Resources\StatusResource;
use App\Http\Resources\UserResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'short_description' => $this->short_description,
            'status' => new StatusResource($this->status),
            'priority' => new PriorityResource($this->priority),
            'user' => new UserResource($this->user),
            'tracker' => new TrackerResource($this->tracker),
            'due_time' => $this->due_time,
            'due_time_for_user' => Carbon::parse($this->due_time)->diffForHumans(),
            'deleted_at' => $this->deleted_at->diffForHumans()
        ];
    }
}
