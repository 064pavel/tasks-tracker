<?php

namespace App\Console\Commands;

use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateTaskStatusCommand extends Command
{
    protected $signature = 'tasks:update_status';

    protected $description = 'Updates the status of tasks that have an upcoming deadline';

    public function handle(): void
    {
        $currentTime = Carbon::now();

        $tasksToUpdate = Task::where('due_time', '<=', $currentTime)
            ->where('status_id', '<>', 7)
            ->get();

        foreach ($tasksToUpdate as $task) {

                Task::where('id', $task->id)
                    ->update(['status_id' => 7]);
        }

        $this->info('Issue status updated.');
    }
}
